//
//  RealmManager.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

typealias updateBlock = () -> Void

class RealmManager {
    static let shared = RealmManager()
    fileprivate let realm = try! Realm()
    
    private init() { self.configuration() }
    
    
    func configuration() {
        
        // MARK: Realm configuration
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            schemaVersion: 0,
            
            migrationBlock: { migration, oldSchemaVersion in
                
                //if oldSchama less than schemaVersion do migration
                if (oldSchemaVersion < 0) {
                    // Realm will automatically detect new properties and removed properties
                    
                    // But if you want to rename old properties so that old value remained, use:
                    //migration.renameProperty(onType: RealmClassThatNeedingChange.className(), from: "oldPropertyName", to: "newPropertyName")
                    
                    // If you want to use old properies value to create new one than use:
                    // don't forget to change oldObjectPropertyName and newObjectPropertyName to your propoerties' names
                    /* migration.enumerateObjects(ofType: RealmClassThatNeedingChange.className(), { (oldObject, newObject) in
                     let someOldObjetValue = oldObject!["oldObjectPropertyName"] as! String
                     newObject!["newObjectPropertyName"] = someOldObjetValue
                     }) */
                }
        })
        
        Realm.Configuration.defaultConfiguration = config
        
        let _ = try! Realm()
    }
    
    func addObject(object: Object)
    {
        try! self.realm.write{
            realm.add(object)
        }
    }
    
    func getAllObjectsOf(type: Object.Type) -> Results<Object> {
        return realm.objects(type)
    }
    
    func deleteObject(object: Object)
    {
        try! self.realm.write{
            realm.delete(object)
        }
    }
    
    func deleteAllObjects()
    {
        try! self.realm.write{
            realm.deleteAll()
        }
    }
    
    func update(block: updateBlock)
    {
        try! self.realm.write {
            block()
        }
    }
}
