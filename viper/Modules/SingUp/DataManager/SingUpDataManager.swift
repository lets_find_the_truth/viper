//
//  SingUpDataManager.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class SingUpDataManager {}

extension SingUpDataManager: SingUpDataManagerProtocol {
    
    func createUser(name: String, email: String, password: String,  completion: (_ user: User) -> Void) {
        UsersService().createUser(name: name, email: email, password: password, completion: completion)
    }
}
        
