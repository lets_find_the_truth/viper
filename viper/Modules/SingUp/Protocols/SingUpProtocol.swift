//
//  SingUpProtocol.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import UIKit


protocol SingUpViewProtocol {
    func showAlert(title: String, message: String)
    func hideHUD()
    func showHUD()
}

protocol SingUpViewPresenterProtocol {
    func singUp(name: String, email: String, password: String, confirmPassword:String)
    func toPreviousScreen()
}

protocol SingUpInteractorPresenterProtocol {
    func signUpFailed(error: CustomError)
    func signUpSuccessed(user: User)
}

protocol SingUpPresenterInteractorProtocol {
    func singUp(name: String, email: String, password: String, confirmPassword:String)
}

protocol SingUpDataManagerProtocol {
    func createUser(name: String, email: String, password: String,  completion: (_ user: User) -> Void)
}

protocol SingUpWireframeProtocol {
    func showPreviousScreen()
    func showProfileScreenWith(user: User)
}
