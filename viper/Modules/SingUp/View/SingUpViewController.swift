//
//  SingUpViewController.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class SingUpViewController: UIViewController{
    
    // MARK: @IBOutlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var confirmPasswordTextField: UITextField!
    
    // MARK: Properties
    
    var presenter: SingUpViewPresenterProtocol!

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: @IBActions
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        showHUD()
        presenter.singUp(name: nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, confirmPassword: confirmPasswordTextField.text!)
    }
    
    @IBAction func didTapBackButton(_ sender: UIBarButtonItem) {
        presenter.toPreviousScreen()
    }
}


extension SingUpViewController: SingUpViewProtocol {
    
    func showAlert(title: String, message: String) {
        Alert(title: title, message: message, style: .alert).addAction(title: "OK", style: .cancel, handler: nil).show()
    }
    
    func hideHUD() {
        Utilities.shared.hideHUD()
    }
    
    func showHUD() {
        Utilities.shared.showHUD()
    }
}

