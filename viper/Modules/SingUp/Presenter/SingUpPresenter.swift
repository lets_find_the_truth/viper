//
//  SingUpPresenter.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import UIKit


class SingUpPresenter {

    // MARK: Properties
    
    var view: SingUpViewProtocol!
    var wireframe: SingUpWireframeProtocol!
    var interactor: SingUpPresenterInteractorProtocol!
}

extension SingUpPresenter: SingUpViewPresenterProtocol {
    
    func singUp(name: String, email: String, password: String, confirmPassword:String) {
        interactor.singUp(name: name, email: email, password: password, confirmPassword: confirmPassword)
    }
    
    func toPreviousScreen() {
        wireframe.showPreviousScreen()
    }
}

extension SingUpPresenter: SingUpInteractorPresenterProtocol {
    
    func signUpSuccessed(user: User) {
        view.hideHUD()
        wireframe.showProfileScreenWith(user: user)
    }
    
    func signUpFailed(error: CustomError) {
        view.hideHUD()
        view.showAlert(title: error.localizedTitle, message: error.localizedDescription)
    }
}
