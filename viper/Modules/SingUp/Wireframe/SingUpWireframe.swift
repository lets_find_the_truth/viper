//
//  SingUpWireframe.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class SingUpWireframe {

    // MARK: Properties
   
    var view: UIViewController!

    // MARK: Static methods
    
    static func setupModule() -> SingUpViewController {
        let identifier = "SingUp"
        
        let view =  UIStoryboard(name: "UserFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier) as! SingUpViewController
        let presenter = SingUpPresenter()
        let wireframe = SingUpWireframe()
        let interactor = SingUpInteractor()
        let dataManager = SingUpDataManager()
        
        view.presenter =  presenter
        
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        
        wireframe.view = view
        
        interactor.presenter = presenter
        interactor.dataManager = dataManager
        
        return view
    }
}


extension SingUpWireframe: SingUpWireframeProtocol {
    
    func showPreviousScreen() {
        _ = view.navigationController?.popViewController(animated: true)
    }
    
    func showProfileScreenWith(user: User) {
        let profileViewController = ProfileWireframe.setupModuleWith(user: user)
        Utilities.shared.showAsRoot(viewController: profileViewController)
    }
}
