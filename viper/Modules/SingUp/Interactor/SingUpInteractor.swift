//
//  SingUpInteractor.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class SingUpInteractor {

    // MARK: Properties
    
    var presenter: SingUpInteractorPresenterProtocol!
    var dataManager: SingUpDataManagerProtocol!
}

extension SingUpInteractor: SingUpPresenterInteractorProtocol {
    
    func singUp(name: String, email: String, password: String, confirmPassword:String) {
        if name.isValidName() && email.isValidName() && password.isValidPassword() && password == confirmPassword {
            dataManager.createUser(name: name, email: email, password: password, completion: { (user) in
                presenter.signUpSuccessed(user: user)
            })
        } else {
            let error:CustomError = CustomError(localizedTitle: "Validation error", localizedDescription: "Name, email, password aren't valid or passwords don't match", code: 0)
            presenter.signUpFailed(error: error)
        }
    }
}


   
        
