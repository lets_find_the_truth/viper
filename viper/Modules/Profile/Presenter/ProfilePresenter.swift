//
//  ProfilePresenter.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class ProfilePresenter {

    // MARK: Properties

    var view: ProfileViewProtocol!
    var wireframe: ProfileWireframeProtocol!
    var interactor: ProfilePresenterInteractorProtocol!
}

extension ProfilePresenter: ProfileViewPresenterProtocol {
    
    func logOut() {
        wireframe.showLogoutAlert { (isCanceled) in
            Utilities.shared.hideHUD()
            guard !isCanceled else {
                return
            }
            
            self.interactor.logOut()
        }
    }
       
    func setupView() {
        interactor.getCurrentUser()
    }
    
    func saveUserData(email: String, name: String) {
        interactor.saveUserData(email: email, name: name)
    }
}

extension ProfilePresenter: ProfileInteractorPresenterProtocol {
    
    func logedOut() {
        wireframe.showLoginScreen()
    }
    
    func userSaveSuccessed() {
        view.hideHUD()
        view.showAlert(title: "Save user", message: "Saving succesed")
    }
    
    func userSaveFailed(error: CustomError) {
        view.hideHUD()
        view.showAlert(title: error.localizedTitle, message: error.localizedDescription)
    }
    
    func user(user: User) {
        view.fillUserData(email: user.email, name: user.name)
    }
}
