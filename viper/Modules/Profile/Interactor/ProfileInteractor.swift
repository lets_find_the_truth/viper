//
//  ProfileInteractor.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class ProfileInteractor {

    // MARK: Properties
    
    var presenter: ProfileInteractorPresenterProtocol!
    var dataManager: ProfileDataManagerProtocol!
}


extension ProfileInteractor: ProfilePresenterInteractorProtocol {
    
    func logOut() {
        dataManager.logOut { 
            presenter.logedOut()
        }
    }
    
    func getCurrentUser() {
        dataManager.getCurrentUser { (user) in
            presenter.user(user: user)
        }
    }

    func saveUserData(email: String, name: String) {
        if email.isValidEmail() && name.isValidName() {
            dataManager.saveUserData(email: email, name: name, completion: { 
                presenter.userSaveSuccessed()
            })
        } else {
            let error:CustomError = CustomError(localizedTitle: "Validation error", localizedDescription: "Name and/or email aren't valid", code: 0)
            presenter.userSaveFailed(error: error)
        }
    }
}
