//
//  ProfileProtocol.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import UIKit


protocol ProfileViewProtocol {
    func fillUserData(email: String, name: String)
    func showAlert(title: String, message: String)
    func hideHUD()
    func showHUD()
}

protocol ProfileViewPresenterProtocol {
    func logOut()
    func setupView()
    func saveUserData(email: String, name: String)
}

protocol ProfileInteractorPresenterProtocol {
    func logedOut()
    func userSaveSuccessed()
    func userSaveFailed(error: CustomError)
    func user(user: User)
}

protocol ProfilePresenterInteractorProtocol {
    func logOut()
    func getCurrentUser()
    func saveUserData(email: String, name: String)
}

protocol ProfileDataManagerProtocol {
    func logOut(completion: () -> Void)
    func getCurrentUser(completion: (_ user: User) -> Void)
    func saveUserData(email: String, name: String, completion: () -> Void)
}

protocol ProfileWireframeProtocol {
    func showLoginScreen()
    func showLogoutAlert(completion: @escaping (_ isCanceled: Bool) -> Void)
}
