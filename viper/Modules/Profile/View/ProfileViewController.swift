//
//  ProfileViewController.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class ProfileViewController: UIViewController {
    
    // MARK: @IBOutlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    
    // MARK: Properties
    
    var presenter: ProfileViewPresenterProtocol!
    
    // MARK: Lifecycle
   
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupView()
    }
    
    // MARK: @IBActions
    
    @IBAction func didTapSaveButton(_ sender: UIButton) {
        showHUD()
        presenter.saveUserData(email: emailTextField.text!, name: nameTextField.text!)
    }
    
    @IBAction func didTapLogOutBarButtonItem(_ sender: UIBarButtonItem) {
        presenter.logOut()
    }
}

extension ProfileViewController: ProfileViewProtocol {
    
    func fillUserData(email: String, name: String) {
        emailTextField.text = email
        nameTextField.text = name
    }
    
    func hideHUD() {
        Utilities.shared.hideHUD()
    }
    
    func showHUD() {
        Utilities.shared.showHUD()
    }
    
    func showAlert(title: String, message: String) {
       Alert(title: title, message: message, style: .alert).addAction(title: "OK", style: .cancel, handler: nil).show()
    }
}

