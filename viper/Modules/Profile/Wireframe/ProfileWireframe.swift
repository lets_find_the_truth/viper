//
//  ProfileWireframe.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class ProfileWireframe {

    // MARK: Properties

    weak var view: UIViewController?

    // MARK: Static methods

    static func setupModuleWith(user: User!) -> UIViewController {
        let identifier = "Profile"
        
        let view =  UIStoryboard(name: "Dashboard", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier) as! ProfileViewController
        let navigation = UINavigationController(rootViewController: view)
        
        let presenter = ProfilePresenter()
        let wireframe = ProfileWireframe()
        let interactor = ProfileInteractor()
        let dataManager = ProfileDataManager()
        
        view.presenter =  presenter
        
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        
        wireframe.view = view
        
        interactor.presenter = presenter
        interactor.dataManager = dataManager
        
        dataManager.currentUser = user
     
        return navigation
    }
}

extension ProfileWireframe: ProfileWireframeProtocol {
    
    func showLoginScreen() {
        let loginViewController = LoginWireframe.setupModule()
        Utilities.shared.showAsRoot(viewController: loginViewController)
    }
    
    func showLogoutAlert(completion: @escaping (Bool) -> Void) {
        Alert(title: "Log out", message: "Are you sure you want to log out?",style: .alert).addAction(title: "Log out", style: .default) { (action) in
                Utilities.shared.showHUD()
                completion(false)
            }.addAction(title: "Cancel", style: .cancel) { (action) in
                completion(true)
            }.show()
    }
}
