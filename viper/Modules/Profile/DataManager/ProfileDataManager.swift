//
//  ProfileDataManager.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class ProfileDataManager {

    // MARK: Properties
    
    var currentUser: User?
}

extension ProfileDataManager: ProfileDataManagerProtocol {
    
    func saveUserData(email: String, name: String, completion: () -> Void) {
        UsersService().updateUser(name: name, email: email, user: self.currentUser!, completion: completion)
    }
    
    func logOut(completion: () -> Void) {
        self.currentUser = nil
        completion()
    }

    func getCurrentUser(completion: (User) -> Void) {
        completion(self.currentUser!)
    }
}
