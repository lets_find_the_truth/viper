//
//  LoginWireframe.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class LoginWireframe {

    // MARK: Properties

    weak var view: UIViewController!

    // MARK: Static methods

    static func setupModule() -> UIViewController {
        let identifier = "Login"
        
        let view =  UIStoryboard(name: "UserFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier) as! LoginViewController
        let navigation = UINavigationController(rootViewController: view)
        let presenter = LoginPresenter()
        let wireframe = LoginWireframe()
        let interactor = LoginInteractor()
        let dataManager = LoginDataManager()
        
        view.presenter =  presenter
        
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        
        wireframe.view = view
        
        interactor.presenter = presenter
        interactor.dataManager = dataManager
        
        return navigation
    }
}


extension LoginWireframe: LoginWireframeProtocol {
    
    func showProfileScreenWith(user: User) {
        let profileViewController = ProfileWireframe.setupModuleWith(user: user)
        Utilities.shared.showAsRoot(viewController: profileViewController)
    }
    
    func showSingUpScreen() {
        let singUpViewController = SingUpWireframe.setupModule()
        view?.navigationController?.pushViewController(singUpViewController, animated: true)
    }
}
