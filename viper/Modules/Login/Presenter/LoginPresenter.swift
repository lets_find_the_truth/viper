//
//  LoginPresenter.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class LoginPresenter {

    // MARK: Properties

    var view: LoginViewProtocol!
    var wireframe: LoginWireframeProtocol!
    var interactor: LoginPresenterInteractorProtocol!
}

extension LoginPresenter: LoginViewPresenterProtocol {
    
    func loginWith(email: String, password: String) {
        interactor.loginWith(email: email, password: password)
    }     
    
    func singUp() {
        wireframe.showSingUpScreen()
    }
}

extension LoginPresenter: LoginInteractorPresenterProtocol {
    
    func loginSuccessed(user: User) {
        view.hideHUD()
        wireframe.showProfileScreenWith(user: user)
    }
    
    func loginFailed(error: CustomError) {
        view.hideHUD()
        view.showAlert(title: error.localizedTitle, message: error.localizedDescription)
    }
}
   
