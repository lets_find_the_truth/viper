//
//  LoginInteractor.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class LoginInteractor {

    // MARK: Properties
    
    var presenter: LoginInteractorPresenterProtocol!
    var dataManager: LoginDataManagerProtocol!
}

extension LoginInteractor: LoginPresenterInteractorProtocol {
    
    func loginWith(email: String, password: String) {
        if email.isValidEmail() && password.isValidPassword() {
            dataManager.loginWith(email: email, password: password, completion: { (user, error) in
                guard error == nil else {
                    presenter.loginFailed(error: error!)
                    return
                }
                
                presenter.loginSuccessed(user: user!)
            })
        } else {
            let error = CustomError(localizedTitle: "Login validation error", localizedDescription: "Email or/and password aren't valid", code: 0)
            presenter.loginFailed(error: error)
        }
    }
}
