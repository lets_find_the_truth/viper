//
//  LoginDataManager.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


class LoginDataManager {}

extension LoginDataManager: LoginDataManagerProtocol {
    
    func loginWith(email: String, password: String, completion: (User?, CustomError?) -> Void) {
        UsersService().loginWith(email: email, password: password, completion:  completion)
    }
}
