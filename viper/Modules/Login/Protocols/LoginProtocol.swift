//
//  LoginProtocol.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation


protocol LoginViewProtocol {
    func showAlert(title: String, message: String)
    func hideHUD()
    func showHUD()
}

protocol LoginViewPresenterProtocol {
    func loginWith(email: String, password: String)
    func singUp()
}

protocol LoginInteractorPresenterProtocol {
    func loginSuccessed(user: User)
    func loginFailed(error: CustomError)
}

protocol LoginPresenterInteractorProtocol {
    func loginWith(email: String, password: String)
}

protocol LoginDataManagerProtocol {
    func loginWith(email: String, password: String, completion: (_ user: User?, _ error: CustomError?) -> Void)
}

protocol LoginWireframeProtocol {
    func showProfileScreenWith(user: User)
    func showSingUpScreen()
}
