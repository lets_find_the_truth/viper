//
//  LoginViewController.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import UIKit


class LoginViewController: UIViewController{
    
    // MARK: @IBOutlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    // MARK: Properties

    var presenter: LoginViewPresenterProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: @IBActions
    
    @IBAction func didTapLoginButton(_ sender: UIButton) {
        showHUD()
        presenter.loginWith(email: emailTextField.text!, password: passwordTextField.text!)
    }
    
    @IBAction func didTapSignUpBarButtonItem(_ sender: UIBarButtonItem) {
        presenter.singUp()
    }
    
}


extension LoginViewController: LoginViewProtocol {
    
    func showAlert(title: String, message: String) {
        Alert(title: title, message: message, style: .alert).addAction(title: "OK", style: .cancel, handler: nil).show()
    }
    
    func hideHUD() {
        Utilities.shared.hideHUD()
    }
    
    func showHUD() {
        Utilities.shared.showHUD()
    }
}

