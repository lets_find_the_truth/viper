//
//  UsersService.swift
//  viper
//
//  Created by Kyryl Nevedrov on 26/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

class UsersService {
    func createUser(name: String, email: String, password: String, completion: (_ user: User) -> Void) {
        let user = User(name: name, email: email, password: password)
        RealmManager.shared.addObject(object: user)
        completion(user)
    }
    
    func updateUser(name: String, email: String, user: User, completion: () -> Void) {
        try! Realm().write {
            user.email = email
            user.name = name
        }
        completion()
    }
    
    func loginWith(email: String, password: String, completion: (_ user: User?, _ error: CustomError?) -> Void) {
        if let user = RealmManager.shared.getAllObjectsOf(type: User.self).filter("email = %@ AND password = %@", email, password).first as? User {
            completion(user, nil)
            
        } else {
            let error = CustomError(localizedTitle: "loginError", localizedDescription: "Email or password are incorect", code: 401)
            completion(nil, error)
        }
    }
    
    func deleteUser(user: User) {
        RealmManager.shared.deleteObject(object: user)
    }
}
