//
//  User.swift
//  viper
//
//  Created by Kyryl Nevedrov on 26/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    dynamic var name: String = ""
    dynamic var email: String = ""
    dynamic var password: String = ""
    
    convenience init(name: String, email: String, password: String) {
        self.init()
        self.name = name
        self.email = email
        self.password = password
    }
}
