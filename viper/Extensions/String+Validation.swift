//
//  String+Validation.swift
//  viper
//
//  Created by Kyryl Nevedrov on 26/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
        
    }
    
    func isValidName() -> Bool  {
        let nameRegEx = "^.{3,}$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
        
    }
    
    func isValidPassword() -> Bool  {
        let passwordRegEx = "^.{6,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
}
