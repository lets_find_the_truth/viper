//
//  CustomError.swift
//  viper
//
//  Created by Kyryl Nevedrov on 26/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation

protocol CustomErrorProtocol: Error {
    
    var localizedTitle: String { get }
    var localizedDescription: String { get }
    var code: Int { get }
}

struct CustomError: CustomErrorProtocol {
    
    var localizedTitle: String
    var localizedDescription: String
    var code: Int
    
    init(localizedTitle: String?, localizedDescription: String, code: Int) {
        self.localizedTitle = localizedTitle ?? "Error"
        self.localizedDescription = localizedDescription
        self.code = code
    }
}
