//
//  Utilities.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import Foundation
import MBProgressHUD
import Async

class Utilities {
    static let shared = Utilities()
    
    private init() {}
    
    // MARK: - variables
    
    private lazy var rootWindow = (UIApplication.shared.delegate as! AppDelegate).window
    
    var hud = MBProgressHUD()
        {
        didSet
        {
            hud.backgroundView.style = .solidColor
            hud.backgroundView.color = UIColor(white: 0, alpha: 0.1)
        }
    }
    
    // MARK: - HUDs
    
    func showHUD()
    {
        Async.main {
            self.hud.hide(animated: true)
            self.hud = MBProgressHUD.showAdded(to: self.rootWindow!, animated: true)
        }
    }
    
    func hideHUD()
    {
        Async.main {
            self.hud.hide(animated: true)
        }
    }
    
    func showHUDWithProgress(message: String, progress: Float)
    {
        Async.main {
            self.hud.hide(animated: true)
            
            self.hud = MBProgressHUD.showAdded(to: self.rootWindow!, animated: true)
            self.hud.mode = .determinateHorizontalBar
            self.hud.label.text = message
            
            guard progress < 1.0 else
            {
                self.hideHUD()
                return
            }
            
            self.hud.progress = progress
        }
    }
    
    // MARK: - Navigation
    
    func showAsRoot(viewController: UIViewController) {
        rootWindow?.rootViewController = viewController
    }
}
