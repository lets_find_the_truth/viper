//
//  Alert.swift
//  viper
//
//  Created by Kyryl Nevedrov on 25/04/17.
//  Copyright © 2017 nevedrov. All rights reserved.
//

import UIKit

class Alert {
    private var alert: UIAlertController!
    private var rootViewController = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
    
    init (title: String, message: String, style: UIAlertControllerStyle)
    {
        self.alert = UIAlertController(title: title, message: message, preferredStyle: style)
    }
    
    func addAction(title: String, style: UIAlertActionStyle, handler: ((UIAlertAction) -> Void)?) -> Alert
    {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        self.alert.addAction(action)
        
        return self
    }
    
    func show()
    {
        rootViewController?.present(self.alert, animated: true, completion: nil)
    }
    
}
